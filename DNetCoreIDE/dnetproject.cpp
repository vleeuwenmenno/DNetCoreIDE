#include "dnetproject.h"
#include <QStringList>
#include <QString>
#include <QFile>
#include <QIODevice>
#include <QByteArray>

DNetProject::DNetProject(QString jsonString)
{
    QJsonDocument sd = QJsonDocument::fromJson(jsonString.toUtf8());
    QJsonObject obj = sd.object();

    projectName = obj.value("projectName").toString();
    frameworkTarget = obj.value("frameworkTarget").toString();
    projectLanguage = obj.value("language").toString();

    for (int i = 0; i < obj.count(); i++)
    {
        if (obj.keys().at(i) != "projectName" && obj.keys().at(i) != "frameworkTarget" && obj.keys().at(i) != "language")
            projectFiles.append(obj.value(obj.keys().at(i)).toString());
    }
}

DNetProject::DNetProject()
{

}

QString DNetProject::toJsonString()
{
    QJsonObject json;

    json["projectName"] = projectName;
    json["frameworkTarget"] = frameworkTarget;
    json["language"] = projectLanguage;

    for (int i = 0; i < projectFiles.count(); i++)
    {
        json[projectFiles[i]] = projectFiles[i];
    }

    QJsonDocument doc(json);
    QString strJson(doc.toJson(QJsonDocument::Indented));

    return strJson;
}

DNetProject DNetProject::readFile(QString filename) {
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
     return DNetProject();
  }

  QByteArray total;
  QByteArray line;
  while (!file.atEnd()) {
     line = file.read(1024);
     total.append(line);
  }

  DNetProject f = DNetProject(QString(total));
  return f;
}

