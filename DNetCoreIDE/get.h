#ifndef GET_H
#define GET_H
#include <QString>

class Get
{
public:
    static QString osName();
    static void delay(int seconds);
    Get();
};

#endif // GET_H
