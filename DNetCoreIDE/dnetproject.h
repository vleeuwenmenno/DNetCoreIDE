#ifndef DNETPROJECT_H
#define DNETPROJECT_H

#include <QString>
#include <QJsonObject>
#include <QJsonDocument>
#include <QList>

class DNetProject
{
public:
    QString projectName;
    QString frameworkTarget;
    QString projectLanguage;
    QList<QString> projectFiles;
    QString toJsonString();

    DNetProject readFile(QString filename);

    DNetProject(QString jsonString);
    DNetProject();

};

#endif // DNETPROJECT_H
