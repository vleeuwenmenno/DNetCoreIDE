#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <dnetproject.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QString projectFile;
    DNetProject project;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_toDoListWidget_visibilityChanged(bool visible);
    void on_projectFilesWidget_visibilityChanged(bool visible);
    void on_compileOutputWidget_visibilityChanged(bool visible);
    void on_outputWidget_visibilityChanged(bool visible);

    void on_projectFilesToggle_toggled(bool arg1);

    void on_todoListToggle_toggled(bool arg1);

    void on_cOutputToggle_toggled(bool arg1);

    void on_outputToggle_toggled(bool arg1);

    void on_projectFiles_doubleClicked(const QModelIndex &index);
    void showEvent( QShowEvent* event ) ;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
