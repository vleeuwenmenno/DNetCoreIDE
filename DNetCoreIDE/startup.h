#ifndef STARTUP_H
#define STARTUP_H

#include <QMainWindow>

namespace Ui {
class Startup;
}

class Startup : public QMainWindow
{
    Q_OBJECT

public:
    QString osName();
    explicit Startup(QWidget *parent = 0);
    ~Startup();

private slots:
    void on_createNewBtn_clicked();

    void on_closeBtn_clicked();

    void on_openGitBtn_clicked();

    void on_openBtn_clicked();

private:
    Ui::Startup *ui;
};

#endif // STARTUP_H
