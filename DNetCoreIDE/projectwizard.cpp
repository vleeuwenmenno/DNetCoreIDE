#include "projectwizard.h"
#include "ui_projectwizard.h"
#include <QFileDialog>
#include <QProcess>
#include <QByteArray>
#include "get.h"
#include <QDebug>
#include <QStyle>
#include <QDesktopWidget>
#include "dnetproject.h"
#include "loading.h"
#include <QMessageBox>
#include <QPixmap>

projectWizard::projectWizard(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::projectWizard)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint);

    this->setFixedSize(this->size());

    ui->pathTxt->setText(QDir::homePath());
    currentPath = QDir::homePath();

    ui->projectLanguage->addItem("C# (CSharp)");
    ui->projectLanguage->addItem("F# (FSharp)");

    ui->projectType->addItem("Console Application");
    ui->projectType->addItem("ASP.NET Core");

    QString prog = "dotnet --version";

    QProcess* process = new QProcess(this);
    process->start(prog);
    process->waitForFinished();
    QString tmp = process->readAll();
    ui->targetFramework->addItem(tmp.trimmed());

    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

projectWizard::~projectWizard()
{
    delete ui;
}

void projectWizard::on_browseBtn_clicked()
{
    QFileDialog *dialog = new QFileDialog(this, "Select project path...", QDir::homePath());
    dialog->setFileMode(QFileDialog::DirectoryOnly);
    dialog->exec();

    if (Get::osName() == "windows")
    {
        ui->pathTxt->setText(dialog->selectedUrls()[0].toString().replace("file:///", ""));
        currentPath = dialog->selectedUrls()[0].toString().replace("file:///", "");
    }
    else if (Get::osName() == "linux")
    {
        ui->pathTxt->setText(dialog->selectedUrls()[0].toString().replace("file://", ""));
        currentPath = dialog->selectedUrls()[0].toString().replace("file://", "");
    }

    if (ui->createDirChk->isChecked() && ui->projectNameTxt->text() != "")
    {
        if (ui->projectNameTxt->text().contains(" ") || ui->projectNameTxt->text().contains("/") || ui->projectNameTxt->text().contains("?") || ui->projectNameTxt->text().contains("\\") || ui->projectNameTxt->text().contains("*") || ui->projectNameTxt->text().contains("|") || ui->projectNameTxt->text().contains("\"") || ui->projectNameTxt->text().contains(":") || ui->projectNameTxt->text().contains(">") || ui->projectNameTxt->text().contains("<"))
        {
            ui->pathTxt->setText(currentPath+"/");
            ui->projectNameTxt->setStyleSheet("color: red;");
        }
        else
        {
            ui->pathTxt->setText(currentPath+"/"+ui->projectNameTxt->text());
            ui->projectNameTxt->setStyleSheet("color: black;");
        }
    }
}

void projectWizard::on_cancelBtn_clicked()
{
    close();
    parentWidget()->show();
}

void projectWizard::on_createDirChk_clicked()
{
    if (ui->createDirChk->isChecked())
    {
        ui->pathTxt->setText(currentPath+"/"+ui->projectNameTxt->text());
    }
    else
    {
        ui->pathTxt->setText(ui->pathTxt->text().mid(0, ui->pathTxt->text().length() - ui->projectNameTxt->text().length() -1));
    }
}

void projectWizard::on_projectNameTxt_textChanged(const QString &arg1)
{
    if (ui->createDirChk->isChecked() && arg1 != "")
    {
        if (arg1.contains(" ") || arg1.contains("/") || arg1.contains("?") || arg1.contains("\\") || arg1.contains("*") || arg1.contains("|") || arg1.contains("\"") || arg1.contains(":") || arg1.contains(">") || arg1.contains("<"))
        {
            ui->pathTxt->setText(currentPath+"/");
            ui->projectNameTxt->setStyleSheet("color: red;");
        }
        else
        {
            ui->pathTxt->setText(currentPath+"/"+arg1);
            ui->projectNameTxt->setStyleSheet("color: black;");
        }
    }
}

void projectWizard::on_createBtn_clicked()
{
    if  (!ui->createDirChk->isChecked() && QFile().exists(currentPath+"/project.json"))
    {
        QMessageBox msg(this);
        msg.setText("This folder already contains a project, please select another folder.");
        msg.setIconPixmap(QPixmap("./Resources/warn.png"));
        msg.setWindowIcon(QIcon(QPixmap("/.Resources/warn.png")));
        msg.exec();
    }
    else if (ui->projectNameTxt->styleSheet().contains("color: red;") || ui->projectNameTxt->text().isEmpty())
    {
        QMessageBox msg(this);
        msg.setText("Project name is invalid.");
        msg.setIconPixmap(QPixmap("./Resources/warn.png"));
        msg.setWindowIcon(QIcon(QPixmap("/.Resources/warn.png")));
        msg.exec();
    }
    else if (!QDir().exists(currentPath))
    {
        QMessageBox msg(this);
        msg.setText("Project path does not exist, please correct the errors.");
        msg.setIconPixmap(QPixmap("./Resources/warn.png"));
        msg.setWindowIcon(QIcon(QPixmap("/.Resources/warn.png")));
        msg.exec();
    }
    else
    {
        Loading *load = new Loading();

        load->projectName = ui->projectNameTxt->text();
        load->projectTargetFramework = ui->targetFramework->currentText();
        load->currentPath = currentPath;
        load->makeDirectory = ui->createDirChk->isChecked();
        load->projectLang = ui->projectLanguage->currentText();
        load->initGit = ui->initGitChk->isChecked();
        load->projectType = ui->projectType->currentText();

        load->show();
    }
}
