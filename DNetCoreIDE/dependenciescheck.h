#ifndef DEPENDENCIESCHECK_H
#define DEPENDENCIESCHECK_H

#include <QDialog>
#include <QProcess>
#include "get.h"
#include <QDebug>
#include <QMessageBox>

namespace Ui {
class DependenciesCheck;
}

class DependenciesCheck : public QDialog
{
    Q_OBJECT

public:
    explicit DependenciesCheck(QWidget *parent = 0);
    void loadCompleted();
    ~DependenciesCheck();

private:
    Ui::DependenciesCheck *ui;

protected:
    void showEvent(QShowEvent *e)
    {
        QDialog::showEvent(e);
        loadCompleted();
    }
};

#endif // DEPENDENCIESCHECK_H
