#ifndef IMPORTGIT_H
#define IMPORTGIT_H

#include <QDialog>

namespace Ui {
class importGit;
}

class importGit : public QDialog
{
    Q_OBJECT

public:
    explicit importGit(QWidget *parent = 0);
    ~importGit();

private slots:
    void on_openBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::importGit *ui;
};

#endif // IMPORTGIT_H
